# Creating Leaves on the BranchKey platform

Go to [the BranchKey application](https://app.branchkey.com). 
> [Create an account](https://app.branchkey.com/register) if you need to 


Use the right hand pane to add a new Branch to your account (with the default settings).

<img src="/docs/create-branch.png"  width="800">

On the next screen you must create 5 leaf accounts each with unique name and password.
> Make note of these leaf name and passwords for later.


<img src="/docs/create-leaves.png"  width="800">
