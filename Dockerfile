FROM ubuntu:22.04

RUN apt-get -y update
RUN apt install -y software-properties-common
RUN apt-get install -y python3
RUN apt-get install -y python3-pip


WORKDIR src

RUN pip3 install jupyterlab
RUN pip3 install numpy
RUN pip3 install matplotlib
RUN pip3 install scipy
RUN pip3 install tqdm
RUN pip3 install ipywidgets
RUN pip3 install branchkey
RUN pip3 install torch --extra-index-url https://download.pytorch.org/whl/cpu

RUN jupyter nbextension enable --py widgetsnbextension


RUN apt-get remove -y python3-pip
RUN apt-get -y auto-remove

COPY jupyter_lab_config.py /root/.jupyter/

COPY engine-sample/ engine/
COPY entrypoint.sh /
COPY dataset/ dataset/
COPY main.ipynb . 

RUN mkdir images
RUN mkdir aggregated_output

EXPOSE 9999

ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]