# BranchKey Client Demo


## Table of Contents

- Setup
  - [Docker](#docker)
  - [Run locally](#local-build)
- [Getting Started](#getting-started)
- [Adding Credentials](#adding-credentials)
- [Run the Example](#run-the-example)
- Extra
  - [Create BranchKey Leafs](/docs/create-branchkey.md)

## Setup

### Docker

```shell
docker pull branchkey/breast-cancer-detection:latest
```
and run
```shell
docker run -d -p 9999:9999 branchkey/breast-cancer-detection:latest
```
now navigate in your browser to:

[http://127.0.0.1:9999](http://127.0.0.1:9999)

Continue to the [Next Step](#getting-started)


### Local Build

Install required packages:
```shell
pip3 install --file requirements.txt
```
> To avoid installing the whole torch library you can comment out torch from the `requirements.txt` and install the CPU only version using: `pip3 install torch --extra-index-url https://download.pytorch.org/whl/cpu`

Next you can simply run:
```shell
jupyter-lab
```
from the root directory to launch the workbook.



## Getting Started
Both setup procedures should land you at the same place:

<img src="/docs/jupyter-dashboard.png"  width="800">

### Adding credentials
> If you do not yet have Leaf name and password accounts [go here](docs/create-branchkey.md)

You will now have access to the `main.ipynb` where you can start the run.

Right click on `group_details.json` >> `open with` >> `editor`

<img src="/docs/edit-json.png"  width="500">


Add your respective detials here. All can be found on your branch dashbaord.

<img src="/docs/group-details.png"  width="500">

Do the same process for each `leaf` in the `agents/` directory. Changing their `name` and `password` the those you set.


## Run the Example

This runs as a normal jupyter notebook. Press the single play button on the top bar to run each cell one at a time and read the explanations along the way. Or click the double play to run the example to the end in one pass.

> Warning: If you do not run the cleanup (even after failed runs or logins) you will lock the leaf session. If your login fails immediately run a cleanup before you try to login again.

<img src="/docs/play.png"  width="1000">
